import TextForm from "./TextForm.vue";

export default {
  title: "과제/2. 입력 폼 UI",
  component: TextForm,
  argTypes: {},
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { TextForm },
  template: "<TextForm v-bind='$props'/>",
});

export const 기본UI = Template.bind({});
기본UI.args = {
  maxLength: 500,
  defaultValue: "",
  readonly: false,
  disabled: false,
};
export const 입력중UI = Template.bind({});
입력중UI.args = {
  maxLength: 500,
  defaultValue: "기본값이 존재할 수도 있어요.",
  readonly: false,
  disabled: false,
};

export const 읽기전용 = Template.bind({});
읽기전용.args = {
  maxLength: 500,
  defaultValue: "",
  readonly: true,
  disabled: false,
};

export const 비활성화 = Template.bind({});
비활성화.args = {
  maxLength: 500,
  defaultValue: "",
  readonly: false,
  disabled: true,
};
