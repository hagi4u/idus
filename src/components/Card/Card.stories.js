import Card from "./Card.vue";

export default {
  title: "과제/1. 카드 UI",
  component: Card,
  argTypes: {
    ratings: {
      control: {
        type: "select",
      },
      options: [0, 1, 2, 3, 4, 5],
    },
    layout: {
      control: { type: "inline-radio" },
      options: ["vertical", "horizontal"],
    },
  },
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { Card },
  template: "<Card v-bind='$props'/>",
});

export const 세로형UI = Template.bind({});
세로형UI.args = {
  layout: "vertical",
  width: 170,
  imagePath:
    "https://i.picsum.photos/id/567/170/170.jpg?blur=2&hmac=1IIvk9iQpvjVuaDnLJhmMUK2OZNDrxviEmT32UGV0w4",
  label: "Card Label",
  title: "Card Title",
  description: "Cross Out",
  highlight: "highlight",
  ratings: 4,
  footerText:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
};

export const 가로형UI = Template.bind({});
가로형UI.args = {
  layout: "horizontal",
  width: 340,
  imagePath:
    "https://i.picsum.photos/id/567/170/170.jpg?blur=2&hmac=1IIvk9iQpvjVuaDnLJhmMUK2OZNDrxviEmT32UGV0w4",
  label: "Card Label",
  title: "Card Title",
  description: "Cross Out",
  highlight: "highlight",
  ratings: 4,
  footerText:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
};
