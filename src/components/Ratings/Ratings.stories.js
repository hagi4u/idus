import Ratings from "./Ratings.vue";

export default {
  title: "컴포넌트/별점",
  component: Ratings,
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { Ratings },
  template: "<Ratings v-bind='$props'/>",
});

export const 기본UI = Template.bind({});
기본UI.args = {
  ratings: 4,
};
